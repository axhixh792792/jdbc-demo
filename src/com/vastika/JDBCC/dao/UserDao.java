package com.vastika.JDBCC.dao;

import java.util.List;

import com.vastika.JDBCC.model.User;

public interface UserDao {

	int saveUserInfo(User user);
	
	List<User> getAllUserInfo();
	
	int updateUserInfo(User user);
	
	int deleteUserInfo(int id);
}

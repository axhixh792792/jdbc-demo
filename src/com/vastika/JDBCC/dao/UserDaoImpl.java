package com.vastika.JDBCC.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.vastika.JDBCC.model.User;
import com.vastika.JDBCC.util.DBUtil;
import com.vastika.JDBCC.util.Queryutil;

public class UserDaoImpl implements UserDao {

	
	
	@Override
	public int saveUserInfo(User user) {
		int saved =0;
		try(PreparedStatement ps = DBUtil.getConnection().prepareStatement(Queryutil.INSERT_SQL)){
			ps.setString(1, user.getUserName());
			ps.setInt(2, user.getAge());
			
			saved =ps.executeUpdate();
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return saved;

		
	}

	@Override
	public List<User> getAllUserInfo() {
		
		List<User> userList = new ArrayList<>();

		try(PreparedStatement ps = DBUtil.getConnection().prepareStatement(Queryutil.LIST_SQL)){
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUserName(rs.getString("user_name"));
				user.setAge(rs.getInt("age"));
				userList.add(user);
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public int updateUserInfo(User user) {
		int updated=0;
		try(PreparedStatement ps = DBUtil.getConnection().prepareStatement(Queryutil.UPDATE_SQL)){
			ps.setString(1, user.getUserName());
			ps.setInt(2, user.getAge());
			ps.setInt(3, user.getId());
			updated=ps.executeUpdate();
			System.out.println("Data updated");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updated;
		
	}

	@Override
	public int deleteUserInfo(int id) {
		int deleted =0;
		
		try(PreparedStatement ps = DBUtil.getConnection().prepareStatement(Queryutil.DELETE_SQL)){
			ps.setInt(1, id);		
			deleted=ps.executeUpdate();
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return deleted;
		
	}

}

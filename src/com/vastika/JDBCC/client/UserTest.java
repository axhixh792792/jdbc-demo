package com.vastika.JDBCC.client;

import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;

import com.vastika.JDBCC.dao.UserDao;
import com.vastika.JDBCC.dao.UserDaoImpl;
import com.vastika.JDBCC.model.User;

public class UserTest {
	public static void main(String[] args) {
		UserDao userDao = new UserDaoImpl();  // interface ko variable ,implementing class ko object
		
		//Scanner input = new Scanner(System.in);
	
		
		
		
		String decision ="";
		
		do {
						
			String choice = JOptionPane.showInputDialog("Which DB operation do you want to perform :?");
			switch (choice) {
			case "save":
				//System.out.println("Enter user name: ");
				String username = JOptionPane.showInputDialog("Enter user name: ")    ;        // temporary username and age for one particular instance object
				
				int age = Integer.parseInt(JOptionPane.showInputDialog("Enter age: ") )   ;        // temporary username and age for one particular instance object
								
				
				User user = new User(); // create an instance of User class
				user.setUserName(username);
				user.setAge(age);
				
				int saved =userDao.saveUserInfo(user); // calling method of interface
				if (saved>=1) {
					JOptionPane.showMessageDialog(null, "User info is saved in DB");
				}else {
					
					JOptionPane.showMessageDialog(null, "error saving info in DB");
				}
				
				break;
				
			case "list":
				List<User> userList = userDao.getAllUserInfo();
				userList.forEach(u -> {
					JOptionPane.showMessageDialog(null,"Id is: "+ u.getId()+"\n"+"Username is : "+ u.getUserName()+ "\n"+ "Age is : "+ u.getAge());
					//JOptionPane.showMessageDialog(null,"Username is : "+ u.getUserName());
					//JOptionPane.showMessageDialog(null,"Age is : "+ u.getAge());
					
				});
				
				break;
				
			case "update":
				
				int id = Integer.parseInt(JOptionPane.showInputDialog("Enter the id you want to update: ") ) ; 
				String uname = JOptionPane.showInputDialog("Enter user name: ");          // temporary username and age for one particular instance object
				int agee = Integer.parseInt(JOptionPane.showInputDialog("Enter age: ") )   ; 
				
				
				User uu = new User(); // create an instance of User class
				uu.setUserName(uname);
				uu.setAge(agee);
				uu.setId(id);
				
				int updated =userDao.updateUserInfo(uu); // calling method of interface
				if (updated>=1) {
					
					JOptionPane.showMessageDialog(null, "User info is updated in DB");
				}else {
					
					JOptionPane.showMessageDialog(null, "error updating info in DB");
				}
				break;
				
			case "delete":
				System.out.println("Enter the id you want to delete: ");
				int idd = Integer.parseInt(JOptionPane.showInputDialog("Enter the id you want to delete: ") ) ; 
							
				int deleted=userDao.deleteUserInfo(idd); // calling method of interface
				if (deleted>=1) {
					JOptionPane.showMessageDialog(null, "User info is deleted in DB");
				}else {
					
					JOptionPane.showMessageDialog(null, "Error deleting user info in DB");
				}
				break;

			default:
				
				JOptionPane.showMessageDialog(null,"Wrong choice") ; 
			}
			
			decision = JOptionPane.showInputDialog("Do you want to perform next operation?") ; 
			
		}while(decision.equalsIgnoreCase("yes"));
		
		JOptionPane.showInputDialog("Thank You!!") ; 
		
		
		
	}
}

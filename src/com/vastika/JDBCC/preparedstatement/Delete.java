package com.vastika.JDBCC.preparedstatement;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.vastika.JDBCC.util.DBUtil;

public class Delete {
	public static final String SQL="delete from user_tbl where id =? ";

	public static void main(String[] args) {
		
		try(PreparedStatement ps = DBUtil.getConnection().prepareStatement(SQL)){
			ps.setInt(1, 10);		
			ps.executeUpdate();
			System.out.println("Data deleted");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
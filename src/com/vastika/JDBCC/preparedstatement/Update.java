package com.vastika.JDBCC.preparedstatement;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.vastika.JDBCC.util.DBUtil;

public class Update {

	public static final String SQL="update user_tbl set user_name =?, age=? where id =?";


	public static void main(String[] args) {
		
		try(PreparedStatement ps = DBUtil.getConnection().prepareStatement(SQL)){
			ps.setString(1, "Krishna Gautam");
			ps.setInt(2, 26);
			ps.setInt(3, 9);
			ps.executeUpdate();
			System.out.println("Data updated");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}


package com.vastika.JDBCC.preparedstatement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.vastika.JDBCC.util.DBUtil;

public class Select {


	public static final String SQL="select * from user_tbl where id=?";

	public static void main(String[] args) {
		
		try(PreparedStatement ps = DBUtil.getConnection().prepareStatement(SQL)){
			
			ps.setInt(1, 10);	 // sql workbench ma 10 number id ko select garcha	
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				System.out.println("Id is: "+ rs.getInt("id"));
				System.out.println("Username is : "+ rs.getString("user_name"));
				System.out.println("Age is : "+ rs.getInt("age"));
				System.out.println("==============================================");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}

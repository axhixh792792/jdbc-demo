package com.vastika.JDBCC.basic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDB {

	public static final String URL = "jdbc:mysql://localhost:3306/" +
			 "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	public static final String USER_NAME = "root";
	public static final String PASSWORD = "";
	public static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
	public static final String SQL= "create database user_db";
	
	public static void main(String[] args) {
		
		Connection con = null;
		Statement st = null;
		
		try {
			//1. Register the driver
			Class.forName(DRIVER_NAME);
			
			//2. Get the object of connection using DriverManager
			con = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
			
			//3. Get the statement object using connection
			st = con.createStatement();
			
			//4. Execute the query using statement
			st.executeUpdate(SQL);
			System.out.println("Database Created");
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				//5. Close the connection
				con.close();
				st.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}

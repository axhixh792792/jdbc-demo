package com.vastika.JDBCC.basic;

import java.sql.SQLException;
import java.sql.Statement;

import com.vastika.JDBCC.util.DBUtil;

public class Update {

	public static final String SQL= "update user_tbl set user_name ='Hari Thapa', age=33 where id =3";

	public static void main(String[] args) {
		
		
		try {
			
			Statement st = DBUtil.getConnection().createStatement();
			
			//OR
			//Connection con = DBUtil.getConnection();
			// st = con.createStatement();
			
			st.executeUpdate(SQL);
			System.out.println("updated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

package com.vastika.JDBCC.basic;

import java.sql.SQLException;
import java.sql.Statement;

import com.vastika.JDBCC.util.DBUtil;

public class Insert {
	
	public static final String SQL= "insert into user_tbl(user_name, age)values('Shyam Thapa', 25) ";

	public static void main(String[] args) {
		
		
		try {
			
			Statement st = DBUtil.getConnection().createStatement();
			
			//OR
			//Connection con = DBUtil.getConnection();
			// st = con.createStatement();
			
			st.executeUpdate(SQL);
			System.out.println("Data inserted");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

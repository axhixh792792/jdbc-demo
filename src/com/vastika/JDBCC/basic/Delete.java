package com.vastika.JDBCC.basic;

import java.sql.SQLException;
import java.sql.Statement;

import com.vastika.JDBCC.util.DBUtil;

public class Delete {

	public static final String SQL= "delete from user_tbl where id =13 ";

	public static void main(String[] args) {
		
		
		try {
			
			Statement st = DBUtil.getConnection().createStatement();
			
			//OR
			//Connection con = DBUtil.getConnection();
			// st = con.createStatement();
			
			st.executeUpdate(SQL);
			System.out.println("deleted");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

package com.vastika.JDBCC.basic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.vastika.JDBCC.util.DBUtil;

public class CreateTable {

	public static final String SQL= "create table user_tbl(id int not null auto_increment, user_name varchar(50), age int, primary key(id)) ";

	public static void main(String[] args) {
		
		
		try {
			Connection con = DBUtil.getConnection();    // establish connection
			Statement st = con.createStatement();       // create statement
			
			st.executeUpdate(SQL);						// execute query
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

package com.vastika.JDBCC.basic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.vastika.JDBCC.util.DBUtil;

public class Select {

	public static final String SQL = "select * from user_tbl";
	public static void main(String[] args) {
		
		try {
			Statement st = DBUtil.getConnection().createStatement();
			ResultSet rs =	st.executeQuery(SQL);   // gets data to this resultset rs
			
			while (rs.next()) {
				System.out.println("id is : "+ rs.getInt("id"));
				System.out.println("username is : "+ rs.getString("user_name"));
				System.out.println("age is : "+ rs.getInt("age"));
				System.out.println("-----------------------------");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
